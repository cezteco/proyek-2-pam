# Proyek Akhir PAM

Proyek ini dibuat untuk menyelesaikan tugas proyek dari mata kuliah Pengembangan Aplikasi Mobile yang dikerjakan oleh
Bony Yudha Sinurat (11S17025),
Grady Sianturi (11S17029),
Abednego Sihombing (11S17036),
Maghel Heans Gultom (11S17046).

Proyek ini adalah sebuah aplikasi QR Code Scanner dengan menggunakan database. Fitur aplikasi yang ditawarkan adalah:
* Melakukan scan terhadap QR Code/Bar Code.
* Menyimpan secara otomatis ke dalam database.
* Flash On/Off.
* Menetapkan hasil QR Code sebagai favorit.
